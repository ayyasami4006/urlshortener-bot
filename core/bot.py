import json
import re
import string
import googl
import requests
import telebot
from telebot import AsyncTeleBot

# LOGGING
import logging json

logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)  # Outputs debug messages to console.

API_TOKEN = '144778272:AAGFIbv5m_Bx26nUOUNKrLcnG93oNwMZI1I'
GOOGLE_API_KEY = 'AIzaSyDdqJ1W6sfLslW08sfTO4-jBT60EdWDh4E'

bot = AsyncTeleBot(API_TOKEN)
client = googl.Googl(GOOGLE_API_KEY)


# Handle '/start' and '/help'
@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    bot.send_message(message.chat.id, """\
    Hi there {0}, I am URL Shortener Bot.\n
    I am here to short, or even expand back, your links.\n
    Just send me some and you'll see the magic! {1}\n
    """.format(message.chat.username, '\xe2\x9c\xa8'))  # Sparkles emoji


# Handle all other messages with content_type 'text' (content_types defaults to ['text'])
@bot.message_handler(func=lambda message: True)
def echo_message(message):
    # Find url(s) from text message
    urls = re.findall('(?:http[s]?://|www\.)(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', message.text)
    if urls:
        for url in urls:
            if 'goo.gl' in url:
                expanded_url = client.expand(url)
                if expanded_url['status'] == 'OK':
                    message_text = 'Here is your expanded URL: {0}'.format(expanded_url['longUrl'])
                    bot.send_message(message.chat.id, message_text, disable_web_page_preview=True)
                else:
                    message_text = 'Sorry, your URL can\'t be expanded: {0}'.format(expanded_url['id'])
                    bot.send_message(message.chat.id, message_text, disable_web_page_preview=True)

            else:
                shortened_url = client.shorten(url)
                message_text = 'Here is your shortened URL: {0}'.format(shortened_url['id'])
                bot.send_message(message.chat.id, message_text, disable_web_page_preview=True)
    else:
        message_text = filter(lambda x: x in string.printable, message.text) or 'OK'
        response = requests.get('http://api.program-o.com/v2.3.1/chatbot/?bot_id=6&say={0}'.format(message_text))
        bot.send_message(message.chat.id, json.loads(response.text)['botsay'])

bot.polling()
